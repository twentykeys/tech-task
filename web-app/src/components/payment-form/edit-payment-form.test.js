import React from 'react';
import { shallow } from 'enzyme';
import EditPaymentForm from './edit-payment-form';
import Select from '../select';

describe('EditPaymentForm component', () => {
  const onFormFieldChanged = () => {};
  const onEditPayment = () => {};
  const onDeletePayment = () => {};
  const props = {
    name: 'test',
    amount: '10',
    startDate: '20/12/2020',
    frequency: 'Monthly',
    id: 1,
    onEditPayment,
    onFormFieldChanged,
    onDeletePayment,
  };
  it('should render correctly', () => {
    const component = shallow(
      <EditPaymentForm
        {...props}
        onFormFieldChanged={onFormFieldChanged}
        onEditPayment={onEditPayment}
        onDeletePayment={onDeletePayment}
      />,
    );

    const form = component.find('form');
    expect(form).toBeDefined();

    const formHeading = component.find('h2');
    expect(formHeading).toBeDefined();
    expect(formHeading.text()).toBe(props.name);

    const name = component.find({name: 'name'});
    expect(name).toBeDefined();
    expect(name.props().value).toBe(props.name);

    const amount = component.find({name: 'amount'});
    expect(amount).toBeDefined();
    expect(amount.props().value).toBe(props.amount);

    const startDate = component.find({name: 'startDate'});
    expect(startDate).toBeDefined();
    expect(startDate.props().value).toBe(props.startDate);

    const frequency = component.find(Select);
    expect(frequency).toBeDefined();
    expect(frequency.props().selectedValue).toBe(props.frequency);
  });

  it('should submit form using passed prop function to edit payment', () => {
    const onEditPayment = jest.fn();
    const component = shallow(<EditPaymentForm {...props}
                                               onEditPayment={onEditPayment}/>);

    const form = component.find('form');

    form.simulate('submit');
    expect(onEditPayment).toBeCalled();
  });

  it('should call onFormFieldChanged when change text changes', () => {
    const onFormFieldChanged = jest.fn();
    const component = shallow(<EditPaymentForm {...props}
                                               onFormFieldChanged={onFormFieldChanged}/>);

    const name = component.find({name: 'name'});

    name.simulate('change', {target: {value: 'test2'}});
    expect(onFormFieldChanged).toBeCalled();
  });

  it('should call onDeletePayment when delete payment is clicked', () => {
    const onDeletePayment = jest.fn();
    const component = shallow(<EditPaymentForm {...props}
                                               onDeletePayment={onDeletePayment}/>);

    const form = component.find({name: 'delete'});

    form.simulate('click');
    expect(onDeletePayment).toBeCalled();
  });
});
