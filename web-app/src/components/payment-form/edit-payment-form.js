import React from 'react';
import Types from 'prop-types';

import Select from '../select';

const translations = {
  subTitle: 'If you\'d like to edit your bill you can change the details below',
  name: 'Name',
  amount: 'Amount',
  startDate: 'Start date',
  frequency: 'Frequency',
  save: 'Save',
  delete: 'Delete',
  cancel: 'Cancel',
};

const EditPaymentForm = ({
                           name,
                           amount,
                           startDate,
                           frequency,
                           onFormFieldChanged,
                           onEditPayment,
                           onDeletePayment,
                           onCancelClicked,
                         }) => {
  return (
    <form className="flexbox-form" onSubmit={onEditPayment}>
      <div className="columns">
        <div className="column details">
          <h2>{name}</h2>
          <p>
            {translations.subTitle}
          </p>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <label htmlFor="name">{translations.name}</label>
          <br/>
          <input
            name="name"
            type="text"
            placeholder="Input"
            aria-label="name"
            value={name}
            onChange={onFormFieldChanged}
          />
        </div>
        <div className="column">
          <label htmlFor="amount">{translations.amount}</label>
          <br/>
          <input
            name="amount"
            type="number"
            placeholder="Input"
            aria-label="amount"
            step="0.01"
            value={amount}
            onChange={onFormFieldChanged}
          />
        </div>
        <div className="column">
          <label htmlFor="startDate">{translations.startDate}</label>
          <br/>
          <input
            name="startDate"
            type="date"
            placeholder="Input"
            aria-label="startDate"
            value={startDate}
            onChange={onFormFieldChanged}
          />
        </div>
        <div className="column">
          <label htmlFor="frequency">{translations.frequency}</label>
          <br/>
          <Select
            handleOnChange={onFormFieldChanged}
            selectedValue={frequency}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column submit">
          <button type="submit" className="btn btn-primary">
            {translations.save}
          </button>
        </div>
        <br/>
        <div className="column submit">
          <button
            type="button"
            name="delete"
            className="btn btn-danger"
            onClick={onDeletePayment}
          >
            {translations.delete}
          </button>
        </div>
        <br/>
        <div className="column submit">
          <button
            type="button"
            name="delete"
            className="btn btn-secondary"
            onClick={onCancelClicked}
          >
            {translations.cancel}
          </button>
        </div>
      </div>
    </form>
  );
};

EditPaymentForm.propTypes = {
  name: Types.string,
  amount: Types.string,
  startDate: Types.string,
  frequency: Types.string,
  onEditPayment: Types.func.isRequired,
  onDeletePayment: Types.func.isRequired,
  onFormFieldChanged: Types.func.isRequired,
  onCancelClicked: Types.func.isRequired,
};

EditPaymentForm.defaultProps = {
  name: null,
  amount: null,
  startDate: null,
  frequency: '',
};

export default EditPaymentForm;
