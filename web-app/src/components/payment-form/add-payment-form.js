import React from 'react';
import Types from 'prop-types';

import Select from '../select';

const translations = {
  title: 'Enter your details',
  subTitle: 'Keep track of your household spendings by adding the bills',
  name: 'Name',
  amount: 'Amount',
  startDate: 'Start Date',
  frequency: 'Frequency',
  add: 'Add new payment',
  cancel: 'Cancel',
};

class AddPaymentForm extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      selectedValue: '',
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  handleSelectChange (e) {
    e.preventDefault();
    this.setState(
      {
        selectedValue: e.target.value,
      });
  }

  render () {
    const {onFormSubmit, onCancelClicked} = this.props;
    const {selectedValue} = this.state;

    return (
      <form className="flexbox-form" onSubmit={onFormSubmit}>
        <div className="columns">
          <div className="column details">
            <h2>{translations.title}</h2>
            <p>{translations.subTitle}</p>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <label htmlFor="name">{translations.name}</label>
            <br/>
            <input
              name="name"
              type="text"
              aria-label="name"
              placeholder="Name"
              required/>
          </div>
          <div className="column">
            <label htmlFor="amount">{translations.amount}</label>
            <br/>
            <input
              name="amount"
              type="number"
              placeholder="Amount"
              aria-label="amount"
              step="0.01"
              required
            />
          </div>
          <div className="column">
            <label htmlFor="startDate">{translations.startDate}</label>
            <br/>
            <input
              name="startDate"
              type="date"
              aria-label="startDate"
              placeholder="Start Date"
              required
            />
          </div>
          <div className="column">
            <label htmlFor="frequency">{translations.frequency}</label>
            <br/>
            <Select handleOnChange={this.handleSelectChange}
                    selectedValue={selectedValue}/>
          </div>
        </div>
        <div className="columns">
          <div className="column submit">
            <button type="submit" className="btn btn-primary">
              {translations.add}
            </button>
          </div>
          <br/>
          <div className="column submit">
            <button
              type="button"
              name="delete"
              className="btn btn-secondary"
              onClick={onCancelClicked}
            >
              {translations.cancel}
            </button>
          </div>
        </div>
      </form>
    );
  }

}

AddPaymentForm.propTypes = {
  onFormSubmit: Types.func.isRequired,
  onCancelClicked: Types.func.isRequired,
};

export default AddPaymentForm;
