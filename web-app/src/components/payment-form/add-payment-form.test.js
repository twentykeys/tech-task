import React from 'react';
import { shallow } from 'enzyme';
import AddPaymentForm from './add-payment-form';

describe('AddPaymentForm component', () => {
  const onFormSubmit = () => {};
  it('should render correctly', () => {
    const component = shallow(<AddPaymentForm onFormSubmit={onFormSubmit}/>);

    const form = component.find('form');
    expect(form).toBeDefined();

    const formHeading = component.find('h2');
    expect(formHeading).toBeDefined();
    expect(formHeading.text()).toBe('Enter your details');

    const formDetails = component.find('p');
    expect(formDetails).toBeDefined();
    expect(formDetails.text()).
      toBe('Keep track of your household spendings by adding the bills');
  });

  it('should submit form using passed prop function', () => {
    const onFormSubmit = jest.fn();
    const component = shallow(<AddPaymentForm onFormSubmit={onFormSubmit}/>);

    const form = component.find('form');

    form.simulate('submit');
    expect(onFormSubmit).toBeCalled();
  });
});
