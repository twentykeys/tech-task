import React from 'react';
import { shallow } from 'enzyme';
import Select from './select';

describe('Select component', () => {
  const handleOnChange = () => {};
  const selectedValue = 'Monthly';
  it('should render correctly', () => {
    const component = shallow(<Select selectedValue={selectedValue}
                                      handleOnChange={handleOnChange}/>);

    const select = component.find('select');
    expect(select).toBeDefined();

    const options = component.find('option');
    expect(options.length).toBe(4);
  });

  it('should call handleOnChange function when select is changed', () => {
    const handleOnChange = jest.fn();
    const component = shallow(<Select handleOnChange={handleOnChange}/>);

    const select = component.find('select');

    select.simulate('change');
    expect(handleOnChange).toBeCalled();
  });
});
