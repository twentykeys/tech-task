import React from 'react';
import Types from 'prop-types';

const values = ['Weekly', 'Monthly', 'Annually'];

const Select = ({selectedValue, handleOnChange}) => {
  return (
    <select name="frequency" value={selectedValue} onChange={handleOnChange}>
      <option key="null" value="null">{''}</option>
      {values.map((val) => (
        <option key={val} value={val}>
          {val}
        </option>
      ))}
    </select>
  );
};

Select.propTypes = {
  selectedValue: Types.string,
  handleOnChange: Types.func,
};

Select.defaultProps = {
  selectedValue: '',
  handleOnChange: () => {},
};

export default Select;
