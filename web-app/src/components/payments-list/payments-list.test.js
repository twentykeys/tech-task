import React from 'react';
import { shallow } from 'enzyme';
import PaymentsList from './payments-list';
import ListItem from '../payment-list-item';

describe('PaymentsList component', () => {
  const onListItemClick = () => {};
  const payments = [
    {
      name: 'test',
      amount: '10',
      startDate: '06-12-2020',
      frequency: 'Annually',
      id: 1,
    }, {
      name: 'test',
      amount: '10',
      startDate: '06-12-2020',
      frequency: 'Annually',
      id: 2,
    }];
  it('should render correctly', () => {
    const component = shallow(<PaymentsList payments={payments}
                                            onListItemClick={onListItemClick}/>);

    const list = component.find('ul');
    expect(list).toBeDefined();

    const listItems = component.find(ListItem);
    expect(listItems).toBeDefined();
    expect(listItems.length).toBe(2);
  });
});
