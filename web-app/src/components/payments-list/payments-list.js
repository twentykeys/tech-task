import React from 'react';
import Types from 'prop-types';

import ListItem from '../payment-list-item';

const PaymentsList = ({payments, onListItemClick}) => {
  return (
    <ul>
      {payments.map((payment) => (
        <ListItem
          listItemData={payment}
          key={payment.id}
          onListItemClick={() => onListItemClick(payment.id)}
        />
      ))}
    </ul>
  );
};

PaymentsList.propTypes = {
  payments: Types.arrayOf(
    Types.shape({
      name: Types.string,
      amount: Types.string,
      startDate: Types.string,
      frequency: Types.string,
    }),
  ),
};

export default PaymentsList;
