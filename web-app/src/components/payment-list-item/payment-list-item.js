import React from 'react';
import Types from 'prop-types';

const calculateNextDate = (startDate, frequency) => {
  if (frequency === '' || !frequency) {
    return '-';
  }

  let date = new Date(startDate);
  if (frequency && frequency.toLowerCase() === 'monthly') {
    date = new Date(date.setMonth(date.getMonth() + 1));
  } else if (frequency && frequency.toLowerCase() === 'annually') {
    date = new Date(date.setMonth(date.getMonth() + 12));
  } else if (frequency && frequency.toLowerCase() === 'weekly') {
    date = new Date(date.setHours(date.getHours() + 24 * 7));
  }
  return date.toDateString();
};

const PaymentListItem = ({listItemData, onListItemClick}) => {
  return (
    <li className="payments" onClick={onListItemClick} key={listItemData.id}>
      <div className="payment">
        <span className="title name" style={{float: 'left'}}>
          {listItemData.name}
        </span>
        <span className="title amount" style={{float: 'right'}}>
          £ {listItemData.amount}
        </span>
      </div>
      <br/>
      <div className="payment">
        <span className="sub-title frequency" style={{float: 'left'}}>
          {listItemData.frequency}
        </span>
        <span className="sub-title startDate" style={{float: 'right'}}>
          Next:{' '}
          {calculateNextDate(listItemData.startDate, listItemData.frequency)}
        </span>
      </div>
    </li>
  );
};

PaymentListItem.propTypes = {
  onListItemClick: Types.func.isRequired,
  listItemData: Types.shape({
    name: Types.string,
    amount: Types.string,
    startDate: Types.string,
    frequency: Types.string,
  }).isRequired,
};

export default PaymentListItem;
