import React from 'react';
import { shallow } from 'enzyme';
import PaymentListItem from './payment-list-item';

describe('PaymentListItem component', () => {
  const onListItemClick = () => {};
  const listItemData = {
    name: 'test',
    amount: '10',
    startDate: '06-12-2020',
    frequency: 'Annually',
  };
  it('should render correctly', () => {
    const component = shallow(<PaymentListItem listItemData={listItemData}
                                               onListItemClick={onListItemClick}/>);

    const name = component.find('.name');
    expect(name).toBeDefined();
    expect(name.text()).toBe(listItemData.name);

    const amount = component.find('.amount');
    expect(amount).toBeDefined();
    expect(amount.text()).toBe(`£ ${listItemData.amount}`);

    const startDate = component.find('.startDate');
    expect(startDate).toBeDefined();
    expect(startDate.text()).toBe('Next: Sat Jun 12 2021');

    const frequency = component.find('.frequency');
    expect(frequency).toBeDefined();
    expect(frequency.text()).toBe(listItemData.frequency);
  });

  it('should call onListItemClick function when clicked on listItem', () => {
    const onListItemClick = jest.fn();
    const component = shallow(<PaymentListItem listItemData={listItemData}
                                               onListItemClick={onListItemClick}/>);

    const listItem = component.find('.payments');

    listItem.simulate('click');
    expect(onListItemClick).toBeCalled();
  });
});
