import React from 'react';
import { shallow } from 'enzyme';
import Header from '.';

describe('Header component', () => {
  const headerText = 'My header';
  it('should render correctly', () => {
    const component = shallow(<Header headerText={headerText}/>);

    const header = component.find('.header');
    expect(header).toBeDefined();
    expect(header.text()).toBe(headerText);
  });

  it('should not display header text if header text is not passed', () => {
    const component = shallow(<Header headerText={null}/>);

    const header = component.find('.header');
    expect(header).toBeDefined();
    expect(header.text()).toBe('');
  });
});
