import React from 'react';
import Types from 'prop-types';

const Header = ({headerText}) => {
  return <div className="header">{headerText}</div>;
};

Header.propTypes = {
  headerText: Types.string.isRequired,
};

export default Header;
