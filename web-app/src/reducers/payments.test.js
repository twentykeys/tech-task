import paymentsReducer from './payments';
import {
  ADD_PAYMENT_FAILURE,
  ADD_PAYMENT_SUCCESS, DELETE_PAYMENT_FAILURE, DELETE_PAYMENT_SUCCESS,
  GET_PAYMENT_FAILURE,
  GET_PAYMENT_SUCCESS,
  GET_PAYMENTS_FAILURE,
  GET_PAYMENTS_SUCCESS,
} from '../actions/types';

const initialState = {
  error: null,
  loading: false,
  payment: {},
  payments: [],
};

describe('payments reducer', () => {
  it('returns the initial state', () => {
    expect(paymentsReducer(undefined, {})).toEqual(initialState);
  });

  it('handles add payment request', () => {
    expect(paymentsReducer(initialState, {type: ADD_PAYMENT_SUCCESS})).toEqual({
      ...initialState,
    });
  });

  it('handles add payment request failure', () => {
    expect(paymentsReducer(initialState,
      {type: ADD_PAYMENT_FAILURE, error: 'error'})).toEqual({
      ...initialState,
      error: 'error',
    });
  });

  it('handles get payment request', () => {
    expect(paymentsReducer(initialState,
      {type: GET_PAYMENT_SUCCESS, data: {name: 'tesla'}})).toEqual({
      ...initialState,
      payment: {name: 'tesla'},
    });
  });

  it('handles get payment request failure', () => {
    expect(paymentsReducer(initialState,
      {type: GET_PAYMENT_FAILURE, error: 'error'})).toEqual({
      ...initialState,
      error: 'error',
    });
  });

  it('handles get payments request', () => {
    expect(paymentsReducer(initialState,
      {type: GET_PAYMENTS_SUCCESS, data: [{name: 'tesla'}]})).toEqual({
      ...initialState,
      payments: [{name: 'tesla'}],
    });
  });

  it('handles get payment request failure', () => {
    expect(paymentsReducer(initialState,
      {type: GET_PAYMENTS_FAILURE, error: 'error'})).toEqual({
      ...initialState,
      error: 'error',
    });
  });

  it('handles delete payments request', () => {
    const dummyState = {
      ...initialState,
      payment: {name: 'tesla'},
    };
    expect(
      paymentsReducer(dummyState, {type: DELETE_PAYMENT_SUCCESS, data: {}})).
      toEqual({
        ...initialState,
      });
  });

  it('handles get payment request failure', () => {
    expect(paymentsReducer(initialState,
      {type: DELETE_PAYMENT_FAILURE, error: 'error'})).toEqual({
      ...initialState,
      error: 'error',
    });
  });
});
