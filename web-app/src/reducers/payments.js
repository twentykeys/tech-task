import {
  ADD_PAYMENT_SUCCESS,
  ADD_PAYMENT_FAILURE,
  GET_PAYMENTS_SUCCESS,
  GET_PAYMENT_SUCCESS,
  EDIT_PAYMENT_SUCCESS,
  DELETE_PAYMENT_SUCCESS,
  GET_PAYMENTS_FAILURE,
  GET_PAYMENT_FAILURE,
  EDIT_PAYMENT_FAILURE, DELETE_PAYMENT_FAILURE,
} from '../actions/types';

const initialState = {
  payments: [],
  payment: {},
  error: null,
  loading: false,
};

const paymentsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PAYMENT_SUCCESS:
      return {...state, error: null};
    case ADD_PAYMENT_FAILURE:
    case GET_PAYMENTS_FAILURE:
    case GET_PAYMENT_FAILURE:
    case EDIT_PAYMENT_FAILURE:
    case DELETE_PAYMENT_FAILURE:
      return {...initialState, error: action.error};
    case GET_PAYMENTS_SUCCESS:
      return {...state, payments: action.data};
    case GET_PAYMENT_SUCCESS:
      return {...initialState, payment: action.data};
    case EDIT_PAYMENT_SUCCESS:
      return {...state, payment: action.data};
    case DELETE_PAYMENT_SUCCESS:
      return {...state, payment: action.data};
    default:
      return state;
  }
};

export default paymentsReducer;
