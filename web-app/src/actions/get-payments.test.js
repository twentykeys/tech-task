import configureStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';

import * as actions from '../actions/payments';
import { API_URL, GET_PAYMENTS_FAILURE, GET_PAYMENTS_SUCCESS } from './types';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('get-payments action', () => {
  afterEach(() => fetchMock.restore);

  it('creates SUCCESS when fetching payments succeeds', () => {
    fetchMock.getOnce(`${API_URL}/payments`, {
      body: {data: ['something']},
      headers: {'content-type': 'application/json'},
    });

    const store = mockStore({payments: []});

    return store.dispatch(actions.getPayments()).then(() => {
      expect(store.getActions()).
        toEqual(
          [{data: {data: ['something']}, type: GET_PAYMENTS_SUCCESS}]);
    });
  });

  it('creates FAILURE when fetching payments fails', () => {
    fetchMock.getOnce(`${API_URL}/payments`,
      {throws: new Error('failed to fetch')}, {overwriteRoutes: false});

    const store = mockStore({payments: []});

    return store.dispatch(actions.getPayments()).then(() => {
      expect(store.getActions()).
        toEqual(
          [
            {
              error: 'General error message',
              type: GET_PAYMENTS_FAILURE,
            }]);
    });
  });
});