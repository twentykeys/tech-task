import { ADD_PAYMENT_FAILURE, API_URL } from './types';

export default function addPayment (payment = {}, history = null) {
  return (dispatch) => {
    return fetch(`${API_URL}/payments`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payment),
    }).
      then((response) => response.json()).
      then((json) => history ? history.push('/') : json).
      catch((error) => dispatch(addPaymentFailure(error)));
  };
}

function addPaymentFailure (error) {
  return {type: ADD_PAYMENT_FAILURE, error: 'General error message'};
}
