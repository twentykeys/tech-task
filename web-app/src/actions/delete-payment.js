import {
  API_URL,
  DELETE_PAYMENT_FAILURE,
  DELETE_PAYMENT_SUCCESS,
} from './types';

export default function deletePayment (paymentId) {
  return (dispatch) => {
    return fetch(`${API_URL}/payments/${paymentId}`, {
      method: 'DELETE',
    }).
      then((response) => response.json()).
      then((json) => dispatch(addPaymentSuccess(json))).
      catch((error) => dispatch(addPaymentFailure(error)));
  };
}

function addPaymentSuccess () {
  return {type: DELETE_PAYMENT_SUCCESS};
}

function addPaymentFailure (error) {
  return {type: DELETE_PAYMENT_FAILURE, error: 'General error message'};
}
