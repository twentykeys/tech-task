import { API_URL, EDIT_PAYMENT_FAILURE, EDIT_PAYMENT_SUCCESS } from './types';

export default function editPayment (payment = {}) {
  return (dispatch) => {
    return fetch(`${API_URL}/payments/${payment.id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payment),
    }).
      then((response) => response.json()).
      then((json) => dispatch(addPaymentSuccess(json))).
      catch((error) => dispatch(addPaymentFailure(error)));
  };
}

function addPaymentSuccess (data) {
  return {
    type: EDIT_PAYMENT_SUCCESS,
    data,
  };
}

function addPaymentFailure (error) {
  return {type: EDIT_PAYMENT_FAILURE, error: 'General error message'};
}
