import configureStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';

import * as actions from '../actions/payments';
import { ADD_PAYMENT_FAILURE, API_URL } from './types';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('add-payment action', () => {
  afterEach(() => fetchMock.restore);

  it('creates SUCCESS when adding a payment succeeds', () => {
    fetchMock.postOnce(`${API_URL}/payments`, {});
    const store = mockStore({payments: []});

    return store.dispatch(actions.addPayment({}, null)).then(() => {
      expect(store.getActions()).
        toEqual(
          []);
    });
  });

  it('creates FAILURE when adding a payment fails', () => {
    fetchMock.postOnce(`${API_URL}/payments`,
      {throws: new Error('failed to fetch')}, {overwriteRoutes: false});

    const store = mockStore({payments: []});

    return store.dispatch(actions.addPayment({})).then(() => {
      expect(store.getActions()).
        toEqual(
          [
            {
              error: 'General error message',
              type: ADD_PAYMENT_FAILURE,
            }]);
    });
  });
});