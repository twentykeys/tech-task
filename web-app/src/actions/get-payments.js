import { API_URL, GET_PAYMENTS_FAILURE, GET_PAYMENTS_SUCCESS } from './types';

export default function getPayments () {
  return (dispatch) => {
    return fetch(`${API_URL}/payments`).
      then((response) => response.json()).
      then((json) => dispatch(getPaymentsSuccess(json))).
      catch((error) => dispatch(getPaymentsFailure()));
  };
}

function getPaymentsSuccess (data) {
  return {type: GET_PAYMENTS_SUCCESS, data};
}

function getPaymentsFailure () {
  // Do not care about error message for now.
  return {type: GET_PAYMENTS_FAILURE, error: 'General error message'};
}
