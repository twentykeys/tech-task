import configureStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';

import * as actions from '../actions/payments';
import { API_URL, GET_PAYMENT_FAILURE, GET_PAYMENT_SUCCESS } from './types';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('get-payment action', () => {
  afterEach(() => fetchMock.restore);

  it('creates SUCCESS when getting a payment succeeds', () => {
    fetchMock.getOnce(`${API_URL}/payments/1`, {});
    const store = mockStore({payments: []});

    return store.dispatch(actions.getPayment(1)).then(() => {
      expect(store.getActions()).
        toEqual(
          [{data: {}, type: GET_PAYMENT_SUCCESS}]);
    });
  });

  it('creates FAILURE when getting a payment fails', () => {
    fetchMock.getOnce(`${API_URL}/payments/2`,
      {throws: new Error('failed to fetch')}, {overwriteRoutes: false});

    const store = mockStore({payments: []});

    return store.dispatch(actions.getPayment(2)).then(() => {
      expect(store.getActions()).
        toEqual(
          [
            {
              error: 'General error message',
              type: GET_PAYMENT_FAILURE,
            }]);
    });
  });
});