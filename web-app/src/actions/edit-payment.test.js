import configureStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';

import * as actions from '../actions/payments';
import { API_URL, EDIT_PAYMENT_FAILURE, EDIT_PAYMENT_SUCCESS } from './types';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('edit-payment action', () => {
  afterEach(() => fetchMock.restore);

  it('creates SUCCESS when editing a payment succeeds', () => {
    fetchMock.patchOnce(`${API_URL}/payments/1`, {});
    const store = mockStore({payments: []});

    return store.dispatch(actions.editPayment({id: 1})).then(() => {
      expect(store.getActions()).
        toEqual(
          [{data: {}, type: EDIT_PAYMENT_SUCCESS}]);
    });
  });

  it('creates FAILURE when editing a payment fails', () => {
    fetchMock.patchOnce(`${API_URL}/payments/2`,
      {throws: new Error('failed to fetch')}, {overwriteRoutes: false});

    const store = mockStore({payments: []});

    return store.dispatch(actions.editPayment({id: 2})).then(() => {
      expect(store.getActions()).
        toEqual(
          [
            {
              error: 'General error message',
              type: EDIT_PAYMENT_FAILURE,
            }]);
    });
  });
});