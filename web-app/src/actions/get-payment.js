import { API_URL, GET_PAYMENT_FAILURE, GET_PAYMENT_SUCCESS } from './types';

export default function getPayment (id) {
  return (dispatch) => {
    return fetch(`${API_URL}/payments/${id}`).
      then((response) => response.json()).
      then((json) => dispatch(getPaymentSuccess(json))).
      catch((error) => dispatch(getPaymentFailure(error)));
  };
}

function getPaymentSuccess (data) {
  return {
    type: GET_PAYMENT_SUCCESS,
    data,
  };
}

function getPaymentFailure (error) {
  return {type: GET_PAYMENT_FAILURE, error: 'General error message'};
}
