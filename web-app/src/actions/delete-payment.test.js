import configureStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';

import * as actions from '../actions/payments';
import {
  API_URL,
  DELETE_PAYMENT_FAILURE,
  DELETE_PAYMENT_SUCCESS,
} from './types';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('delete-payment action', () => {
  afterEach(() => fetchMock.restore);

  it('creates SUCCESS when deleting a payment succeeds', () => {
    fetchMock.deleteOnce(`${API_URL}/payments/1`, {});
    const store = mockStore({payments: []});

    return store.dispatch(actions.deletePayment(1)).then(() => {
      expect(store.getActions()).
        toEqual(
          [{type: DELETE_PAYMENT_SUCCESS}]);
    });
  });

  it('creates FAILURE when deleting a payment fails', () => {
    fetchMock.deleteOnce(`${API_URL}/payments/2`,
      {throws: new Error('failed to fetch')}, {overwriteRoutes: false});

    const store = mockStore({payments: []});

    return store.dispatch(actions.deletePayment(2)).then(() => {
      expect(store.getActions()).
        toEqual(
          [
            {
              error: 'General error message',
              type: DELETE_PAYMENT_FAILURE,
            }]);
    });
  });
});