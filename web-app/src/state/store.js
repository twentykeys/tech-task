import { configureStore } from '@reduxjs/toolkit';
import paymentsReducer from '../reducers/payments';

const store = configureStore({
  reducer: {
    paymentData: paymentsReducer,
  },
});

export default store;
