import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import store from './state/store';

import Home from './containers/home';
import AddPayment from './containers/add-payment';
import EditPayment from './containers/edit-payment';

import './App.css';

export default () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/">
          <Route
            path="/"
            render={(props) => <Home {...props} title="Home"/>}
          />
        </Route>
        <Route
          path="/add"
          render={(props) => <AddPayment {...props} title="Add payment"/>}
        />

        <Route
          path="/edit/:id"
          render={(props) => <EditPayment {...props} title="Edit payment"/>}
        />
      </Switch>
    </Router>
  </Provider>
);
