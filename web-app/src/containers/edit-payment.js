import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as PaymentActions from '../actions/payments';

import Header from '../components/header';
import EditPaymentForm from '../components/payment-form/edit-payment-form';

const translations = {
  title: 'Edit a bill',
  cancel: 'Cancel',
};

class EditPayment extends Component {
  constructor (props) {
    super(props);
    this.props = props;
    const {payment, dispatch} = this.props;
    this.state = {
      name: payment.name || null,
      amount: payment.amount || 0,
      startDate: payment.startDate || null,
      frequency: payment.frequency || '',
      id: payment.id || null,
    };

    bindActionCreators(PaymentActions, dispatch);
    this.onEditPayment = this.onEditPayment.bind(this);
    this.onDeletePayment = this.onDeletePayment.bind(this);
    this.onFormFieldChanged = this.onFormFieldChanged.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.onCancelClicked = this.onCancelClicked.bind(this);
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    if (prevState.id !== nextProps.payment.id) {
      const {payment} = nextProps;
      return {
        id: payment.id,
        name: payment.name,
        amount: payment.amount,
        startDate: payment.startDate,
        frequency: payment.frequency,
      };
    }
    return null;
  }

  componentDidMount () {
    const {dispatch} = this.props;
    const action = PaymentActions.getPayment(this.props.match.params.id);
    dispatch(action);
  }

  onEditPayment (e) {
    e.preventDefault();
    const payment = {
      name: e.target.name.value,
      amount: e.target.amount.value,
      startDate: e.target.startDate.value,
      frequency: e.target.frequency.value,
      id: this.props.payment.id,
    };
    const {dispatch} = this.props;
    dispatch(PaymentActions.editPayment(payment));
    this.props.history.push('/');
  }

  onDeletePayment (e) {
    e.preventDefault();
    const {dispatch, payment} = this.props;
    dispatch(PaymentActions.deletePayment(payment.id));
    this.props.history.push('/');
  }

  onFormFieldChanged (e) {
    e.preventDefault();
    const {name, value} = e.target;
    this.setState({
      [name]: value,
    });
  }

  handleOnChange (e) {
    e.preventDefault();
    this.setState({frequency: e.target.value});
  }

  onCancelClicked (e) {
    e.preventDefault();

    this.props.history.push('/');
  }

  render () {
    const {name, amount, startDate, frequency, id} = this.state;
    return (
      <React.Fragment>
        <Header headerText={translations.title}/>
        {this.props.payment.id ? (
            <EditPaymentForm
              name={name}
              amount={amount}
              startDate={startDate}
              frequency={frequency}
              onEditPayment={this.onEditPayment}
              onDeletePayment={this.onDeletePayment}
              onFormFieldChanged={this.onFormFieldChanged}
              handleOnChange={this.handleOnChange}
              onCancelClicked={this.onCancelClicked}
            />
        ) : (
          <React.Fragment>Loading...</React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps (state) {
  const {paymentData} = state;
  const {payment} = paymentData;
  return {payment};
}

export default connect(mapStateToProps)(EditPayment);
