import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as PaymentActions from '../actions/payments';

import AddPaymentForm from '../components/payment-form/add-payment-form';
import Header from '../components/header/header';

const translations = {
  title: 'Add a bill',
  cancel: 'Cancel',
};

class AddPayment extends React.PureComponent {
  constructor (props) {
    super(props);
    this.props = props;
    const {dispatch} = this.props;
    bindActionCreators(PaymentActions, dispatch);

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onCancelClicked = this.onCancelClicked.bind(this);
  }

  onFormSubmit (e) {
    e.preventDefault();
    const {dispatch, history} = this.props;
    dispatch(
      PaymentActions.addPayment(
        {
          name: e.target.name.value,
          amount: e.target.amount.value,
          startDate: e.target.startDate.value,
          frequency: e.target.frequency.value,
        },
        history,
      ),
    );
  }

  onCancelClicked(e) {
    e.preventDefault();

    this.props.history.push('/');
  }

  render () {
    const {error} = this.props;
    return (
      <React.Fragment>
        <Header headerText={translations.title}/>
        <AddPaymentForm onFormSubmit={this.onFormSubmit} onCancelClicked={this.onCancelClicked}/>
      </React.Fragment>
    );
  }
}

function mapStateToProps (state) {
  const {paymentData} = state;
  return {...paymentData};
}

export default connect(mapStateToProps)(AddPayment);
