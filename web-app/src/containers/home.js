import React, { Component } from 'react';
import Types from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as paymentActions from '../actions/payments';

import Header from '../components/header';
import List from '../components/payments-list';

const translations = {
  title: 'Regular payments',
  addBtnText: 'Add a bill',
};

class Home extends Component {
  constructor (props) {
    super(props);
    this.props = props;
    const {dispatch} = props;

    bindActionCreators(paymentActions, dispatch);
    this.addPayment = this.addPayment.bind(this);
    this.onListItemClick = this.onListItemClick.bind(this);
  }

  componentDidMount () {
    const {dispatch} = this.props;

    const action = paymentActions.getPayments();
    dispatch(action);
  }

  addPayment () {
    this.props.history.push('/add');
  }

  onListItemClick (id) {
    this.props.history.push(`/edit/${id}`);
  }

  render () {
    const {paymentData} = this.props;

    const payments = paymentData ? paymentData.payments : [];
    return (
      <React.Fragment>
        <Header headerText={translations.title}/>
        {payments && (
          <List payments={payments} onListItemClick={this.onListItemClick}/>
        )}
        <div className="columns">
          <div className="column submit">
            <button
              type="submit"
              className="btn btn-primary"
              onClick={this.addPayment}
            >
              {translations.addBtnText}
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

Home.propTypes = {
  paymentData: Types.object,
};

export default connect((state) => ({paymentData: state.paymentData}))(Home);
