This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

#### Disclaimer
I personally wouldn't use redux for this kind of application as data isn't complex but I understand the purpose of being asked to use redux.

Following are the things that I would wish to add if I spent more time on this:
[ ] Loading states for API calls
[ ] Multiple reducers to split the payments data.
[ ] More testing

